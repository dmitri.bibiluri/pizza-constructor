const inputsCheckbox = document.querySelectorAll(
  ".container-custom-checkbox input"
);
ingridients = document.querySelectorAll(".current-pizza-item");
drinks = document.querySelectorAll(".select-drink-item");
totalAmount = document.querySelector(".total-amount>.summa");
orderBTN = document.querySelector(".typical-btn");
modlaWindow = document.querySelector(".modal-window");
submitBTN = document.querySelector(".modal-window__submit-btn");
subject = document.querySelector(".modal-window__subject");
ingredientsSpan = document.querySelector(".modal-window__ingredients");
drinkSpan = document.querySelector(".modal-window__drinks");

const addIngridients = (checkboxes) => {
  const nodsArray = Array.from(checkboxes);
  const ingredientsArray = Array.from(ingridients);
  ingredientsArray.splice(0, 2);
  for (let node of checkboxes) {
    node.addEventListener("click", (event) => {
      event.target.parentNode.classList.toggle("active");
      const index = nodsArray.indexOf(event.target);
      ingredientsArray[index].classList.toggle("active");
      calculateOrder();
    });
  }
};
addIngridients(inputsCheckbox);

const addDrinks = (drinkItems) => {
  for (let item of drinkItems) {
    item.addEventListener("click", (event) => {
      event.target.parentNode.classList.toggle("active");
      calculateOrder();
    });
  }
};

addDrinks(drinks);

const calculateOrder = () => {
  const ingredients = document.querySelectorAll(
    ".container-custom-checkbox.active"
  );
  drinks = document.querySelectorAll(".select-drink-item.active");
  const startPrice = 300;
  ingredientsPrice = ingredients.length * 25;
  drinksPrice = drinks.length * 95;
  totalAmount.innerHTML = `${startPrice + ingredientsPrice + drinksPrice}₽`;
};

window.addEventListener("click", (event) => {
  if (event.target === modlaWindow) {
    modlaWindow.classList.add("none");
  }
});

submitBTN.addEventListener("click", () => {
  modlaWindow.classList.add("none");
});

const prepareWindowModalContent = () => {
  subject.innerHTML = "";
  ingredientsSpan.innerHTML = "";
  drinkSpan.innerHTML = "";

  const addedIngredients = document.querySelectorAll(
    ".container-custom-checkbox.active"
  );
  addedDrinks = document.querySelectorAll(".select-drink-item.active");

  let ingredientsList = [];
  if (addedIngredients) {
    for (ingredient of addedIngredients) {
      ingredientsList.push(ingredient.innerText);
    }
  }

  let drinksList = [];
  if (addedDrinks) {
    for (drink of addedDrinks) {
      drinksList.push(drink.dataset.name);
    }
  }
  const totalIngredients = ingredientsList.join(", ") || "нет ингредиентов";
  const totalDrinks = drinksList.join(", ") || "нет напитков";
  const totalText = `Вы заказали пиццу с ингредиентами:  '${totalIngredients}', а также напитки: '${totalDrinks}', с Вас  ${totalAmount.innerHTML}`;
  subject.innerHTML = totalText;
};

orderBTN.addEventListener("click", () => {
  modlaWindow.classList.remove("none");
  prepareWindowModalContent();
});
